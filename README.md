[![version](https://img.shields.io/badge/version-1.0.1-green.svg)](https://semver.org)

# JAM - Job Application Management

Application to help manage applications for a job or internship research.

## Installation


```node
npm install
```

## Compiles and minifies for production

```node
vue-cli-service build
```

## Compiles and hot-reloads for development

```node
npm run serve
```
## Lints and fixes files

```node
npm run lint
```

# Libraries

Notifications (toast) : https://github.com/Maronato/vue-toastification

# Docker

[Guide deployement](https://cli.vuejs.org/guide/deployment.html#docker-nginx)

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

# License
[MIT](https://gitlab.com/cblondel/jam/-/blob/master/LICENSE)
