import { required, confirmed, email, min } from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("required", {
  ...required,
  message: "This field is required"
});

extend("email", {
  ...email,
  message: "This field must be a valid email"
});

extend("confirmed", {
  ...confirmed,
  message: "Passwords are not matching"
});

extend("min", {
  ...min,
  params: ["length"],
  message: "This field must be at least {length} characters"
});
