import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import vuetify from "./plugins/vuetify";
import "./assets/sass/styles.sass";
import "./assets/js/validators/vee-validate";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import {
  faUser,
  faSearch,
  faLock,
  faAt,
  faPlus,
  faBriefcase,
  faEdit,
  faSave,
  faUndo,
  faSyncAlt,
  faBuilding,
  faMapMarkerAlt,
  faStream,
  faList,
  faGlobe,
  faStickyNote,
  faTrash,
  faExternalLinkAlt
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faUser,
  faSearch,
  faLock,
  faAt,
  faPlus,
  faBriefcase,
  faEdit,
  faSave,
  faUndo,
  faSyncAlt,
  faBuilding,
  faMapMarkerAlt,
  faStream,
  faList,
  faGlobe,
  faStickyNote,
  faTrash,
  faExternalLinkAlt
);

axios.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    if (error.response.status === 401) {
      localStorage.clear();
      store.dispatch("auth/logout");
      router.push("/login");
    } else if (
      error.response.status === 403 &&
      !error.response.data.preventRedirect
    ) {
      router.push("/403");
    } else if (
      error.response.status === 404 &&
      !error.response.data.preventRedirect
    ) {
      router.push("/404");
    } else {
      return Promise.reject(error);
    }
  }
);

Vue.config.productionTip = false;
Vue.component("font-awesome-icon", FontAwesomeIcon);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
