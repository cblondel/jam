/* eslint-disable @typescript-eslint/camelcase */
import ContactModel from "./contact";

export default class ApplicationModel {
  _id: string;
  userId: string;
  companyName: string;
  job: string;
  jobDescription: string;
  website: string;
  notes: string;
  location: string;
  contacts: Array<ContactModel>;
  status: string;
  statusItem: string;
  whenCreated: Date;
  whenUpdated: Date;
  whenDeleted: Date;
  editing: boolean;

  constructor(app: any) {
    this._id = app._id;
    this.userId = app.user_id;
    this.companyName = app.company_name;
    this.job = app.job;
    this.jobDescription = app.job_description;
    this.website = app.website;
    this.notes = app.notes;
    this.location = app.location;
    this.status = app.status;
    this.whenCreated = app.when_created;
    this.whenUpdated = app.when_updated;
    this.whenDeleted = app.when_deleted;
    this.editing = false;

    if (app.status_item.length) this.statusItem = app.status_item[0].name;
    else this.statusItem = "";

    this.contacts = [];
    if (app.contacts)
      app.contacts.forEach((element: any) => {
        this.contacts.push(new ContactModel(element));
      });
  }

  static createEmptyApplication(): ApplicationModel {
    return new ApplicationModel({
      _id: "",
      user_id: "",
      company_name: "",
      job: "",
      jobDescription: "",
      website: "",
      notes: "",
      location: "",
      contacts: new Array<ContactModel>(),
      status: null,
      status_item: "",
      when_created: "",
      when_updated: "",
      when_deleted: "",
      editing: false
    });
  }
}
