export default class ContactModel {
  id: string;
  name: string;
  email: string;
  job: string;

  constructor(contact: any) {
    this.id = contact._id;
    this.name = contact.name;
    this.email = contact.email;
    this.job = contact.job;
  }
}
