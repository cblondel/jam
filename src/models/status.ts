export default class StatusModel {
  id: string;
  name: string;
  count: number;

  constructor(status: any) {
    this.id = status._id;
    this.name = status.name;
    status.count ? (this.count = status.count) : (this.count = 0);
  }
}
