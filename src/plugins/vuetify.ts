import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

Vue.use(Toast, {
    timeout: 2500
});
Vue.use(Vuetify);

export default new Vuetify({});
