import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/vues/Home.vue";
import Login from "@/vues/Security/Login.vue";
import ForgotPassword from "@/vues/Security/ForgotPassword.vue";
import ResetPassword from "@/vues/Security/ResetPassword.vue";
import Register from "@/vues/User/Register.vue";
import Profile from "@/vues/User/Profile.vue";
import Admin from "@/vues/User/Admin.vue";
import Applications from "@/vues/Application/Applications.vue";
import Application from "@/vues/Application/Application.vue";
import Error404 from "@/vues/Security/404.vue";
import Error403 from "@/vues/Security/403.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    redirect: "applications"
  },
  {
    path: "/home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/register",
    name: "register",
    component: Register
  },
  {
    path: "/forgot-password",
    name: "forgot-password",
    component: ForgotPassword
  },
  {
    path: "/reset/:token",
    name: "reset",
    component: ResetPassword
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
    meta: {
      authRequired: true
    }
  },
  {
    path: "/applications",
    name: "applications",
    component: Applications,
    meta: {
      authRequired: true
    }
  },
  {
    path: "/application/:id",
    name: "application",
    component: Application,
    meta: {
      authRequired: true
    }
  },
  {
    path: "/admin",
    name: "admin",
    component: Admin,
    meta: {
      authRequired: true
    }
  },
  {
    path: "/403",
    name: "403",
    component: Error403
  },
  {
    path: "/404",
    name: "404",
    component: Error404
  },
  {
    path: "*",
    redirect: "404"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.Base_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem("user");
  const authRequired = to.matched.some(record => record.meta.authRequired);

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    try {
      console.log("catch ?");
      next("/login");
    } catch (error) {
      console.log("Already on page : ", to.path);
    }
  } else {
    next();
  }
});

export default router;
