import axios from "axios";
import authHeader from "./auth-header";
import * as api from "@/assets/utils/appSettings";

const API_URL = api.URL + "application/";

class ApplicationService {
  get(userId: string, id: string): any {
    return axios.get(API_URL + userId + "/" + id, { headers: authHeader() });
  }

  getAll(userId: string): any {
    return axios.get(API_URL + userId + "/all", { headers: authHeader() });
  }

  save(userId: string, data: {}) {
    return axios.post(API_URL + userId + "/save", data, {
      headers: authHeader()
    });
  }

  update(id: string, data: {}) {
    return axios.put(API_URL + "update/" + id, data, { headers: authHeader() });
  }

  delete(id: string, userID: number) {
    return axios.delete(API_URL + "delete/" + id + "/" + userID, {
      headers: authHeader()
    });
  }

  restore(id: string) {
    return axios.put(API_URL + "restore/" + id, { headers: authHeader() });
  }
}

export default new ApplicationService();
