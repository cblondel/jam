import axios from "axios";
import authHeader from "./auth-header";
import * as api from "@/assets/utils/appSettings";

const API_URL = api.URL + "auth/";

class AuthService {
  login(user: { username: string; password: string }) {
    return axios
      .post(API_URL + "signin", {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(
    user: { username: string; email: string; password: string },
    confirmation: string
  ) {
    return axios.post(API_URL + "signup", {
      username: user.username,
      email: user.email,
      password: user.password,
      confirmation: confirmation
    });
  }

  forgotPassword(email: string) {
    return axios.post(API_URL + "forgot", { email: email });
  }

  reset(token: string, password: string, confirmation: string) {
    return axios.post(API_URL + "reset/" + token, {
      password: password,
      confirmation: confirmation
    });
  }

  updatePassword(id: string, password: string, confirmation: string) {
    return axios.post(
      API_URL + "update/" + id,
      {
        password: password,
        confirmation: confirmation
      },
      { headers: authHeader() }
    );
  }
}

export default new AuthService();
