import axios from "axios";
import authHeader from "./auth-header";
import * as api from "@/assets/utils/appSettings";

const API_URL = api.URL + "status/";

class StatusService {
  get(): any {
    return axios.get(API_URL + "all", { headers: authHeader() });
  }
}

export default new StatusService();
