import axios from "axios";
import authHeader from "./auth-header";
import * as api from "@/assets/utils/appSettings";

const API_URL = api.URL + "user/";

class UserService {
  update(id: string, data: any) {
    return axios.put(API_URL + "update/" + id, data, { headers: authHeader() });
  }

  delete(id: string) {
    return axios.delete(API_URL + "delete/" + id, { headers: authHeader() });
  }
}

export default new UserService();
