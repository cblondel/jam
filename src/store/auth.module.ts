import AuthService from "../services/auth.service";

const user = JSON.parse(localStorage.getItem("user") || "{}");
const initialState =
  user && user.accessToken
    ? { status: { loggedIn: true }, user }
    : { status: { loggedIn: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }: any, user: { username: any; password: any }) {
      return AuthService.login(user).then(
        user => {
          commit("loginSuccess", user);
          return Promise.resolve(user);
        },
        error => {
          commit("loginFailure");
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }: any) {
      AuthService.logout();
      commit("logout");
    },
    register(
      { commit }: any,
      data: {
        user: { username: any; email: any; password: any };
        confirmation: string;
      }
    ) {
      return AuthService.register(data.user, data.confirmation).then(
        response => {
          commit("registerSuccess");
          return Promise.resolve(response.data);
        },
        error => {
          commit("registerFailure");
          console.log("register promise error", error);
          console.log("register promise error.data", error.message);
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    loginSuccess(
      state: { status: { loggedIn: boolean }; user: any },
      user: any
    ) {
      state.status.loggedIn = true;
      state.user = user;
    },
    loginFailure(state: { status: { loggedIn: boolean }; user: null }) {
      state.status.loggedIn = false;
      state.user = null;
    },
    logout(state: { status: { loggedIn: boolean }; user: null }) {
      state.status.loggedIn = false;
      state.user = null;
    },
    registerSuccess(state: { status: { loggedIn: boolean } }) {
      state.status.loggedIn = false;
    },
    registerFailure(state: { status: { loggedIn: boolean } }) {
      state.status.loggedIn = false;
    }
  }
};
